# Camiseta Podcast Linux

Diseño de la camiseta Podcast Linux elegido por los oyentes.
Se libera aquí para que quien quiera lo imprima en su ciudad.

## Diseño

<a rel="Diseño" href="https://gitlab.com/podcastlinux/camiseta/blob/master/CamisetaPL.png">
<img alt="Camiseta Podcast Linux" style="border-width:0" src="https://gitlab.com/podcastlinux/camiseta/raw/master/CamisetaPL.png" />
</a>

## Licencia

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
</a>